@extends('layouts.master')
@section('title')
    Doctors
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Data Doctors</h2>

        </div>
        <div class="card-block">
            <div class="table-responsive dt-responsive">
                <table id="example1" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>KTP</th>
                            <th>Nama Dokter</th>
                            <th>Email</th>
                            <th>Jenis Kelamin</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($doctor as $key => $doctor)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                @if ($doctor->ktp != null)
                                    <td><img class="profile-img" src="{{ asset('ktp/' . $doctor->ktp) }}"
                                            style="width: 40px; "></td>
                                @else
                                    <td><img class="profile-img" src="{{ asset('uploadImage/Profile/profile.jpg') }}"
                                            style="width: 40px;"></td>
                                @endif

                                <td> {{ $doctor->name }} </td>
                                <td> {{ $doctor->email }} </td>
                                <td> {{ $doctor->jeniskelamin }} </td>
                                <td>
                                    <a href="{{ route('doctor.edit', ['doctor' => $doctor]) }}" class="btn btn-info"><i
                                            class="fa fa-pencil"></i> Edit </a>

                                    <form action="{{ route('doctor.destroy', ['doctor' => $doctor]) }}"
                                        style="display: inline;" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger text-light">
                                            <i class="fa fa-trash" aria-hidden="true">
                                                Delete
                                            </i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

    </script>
@endpush
