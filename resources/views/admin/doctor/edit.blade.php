@extends('layouts.master')
@section('title')
    Doctor
@endsection
@section('content')
    <div class="page-wrapper full-calender">
        <div class="page-body">
            <div class="row">


                <div class="row col-lg-12">
                    <h3><b>Edit Doctor</b></h3>
                </div>
                <div class="row col-lg-12">Profil of Doctor : {{ $doctor->name }}<br><br></div>

                <div class="card row col-lg-12">
                    <div class="card-block">
                        <!-- Row start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tab-content card-block">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <form action="{{ route('doctor.update', $doctor->id) }}" method="POST"
                                            data-parsley-validate enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')

                                            <div class="form-group"> <label for="exampleInputEmail1">Name</label> <input
                                                    type="text" class="form-control" id="name" name="name"
                                                    placeholder="Full Name" value="{{ $doctor->name }}" required="true">
                                            </div>
                                            <div class=" form-group"> <label for="exampleInputPassword1">Email</label>
                                                <input type="email" id="email" name="email" class="form-control"
                                                    placeholder="Email" value="{{ $doctor->email }}" required="true">
                                            </div>
                                            <div class="form-group"> <label for="exampleInputEmail1">Mobile
                                                    Number</label> <input type="number" class="form-control" id="mobilenum"
                                                    name="tlp" placeholder="Mobile Number" value="{{ $doctor->tlp }}"
                                                    required="true" maxlength="10" pattern="[0-9]+"> </div>
                                            <div class="radio">
                                                <p style="padding-top: 20px; font-size: 15px"> <strong>Gender:</strong>
                                                    <label>
                                                        <input type="radio" name="jeniskelamin" id="gender"
                                                            value="Perempuan">
                                                        Perempuan
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="jeniskelamin" id="gender"
                                                            value="Laki-laki">
                                                        Laki-laki
                                                    </label>
                                                </p>
                                                @if ($doctor->jeniskelamin != null)
                                                    <label class="bmd-label-floating">Jenis Kelamin | Choosed
                                                        ({{ $doctor->jeniskelamin }})</label>
                                                @else
                                                    <label class="bmd-label-floating">Jenis Kelamin | Not Yet
                                                        Choosed</label>
                                                @endif
                                                <button style="float: right" type="submit" name="submit"
                                                    class="btn btn-default">Update</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
