@extends('layouts.master')
@section('title')
    Dashboard Admin
@endsection
@section('content')
    <div class="page-wrapper full-calender">
        <div class="page-body">
            <div class="row">


                <div class="row col-lg-12">
                    <h3><b>Dashboard</b></h3>
                </div>
                <div class="row col-lg-12">Welcome to Abuya Kangean Hospital<br><br></div>
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-c-green update-card">
                        <div class="card-block">
                            <div class="row align-items-end">
                                <div class="col-8">

                                    <h4 class="text-white">
                                        {{ $patientCount }}
                                    </h4>
                                    <h6 class="text-white m-b-0">Total Pasien</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <canvas id="update-chart-2" height="50"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card bg-c-pink update-card">
                        <div class="card-block">
                            <div class="row align-items-end">
                                <div class="col-8">

                                    <h4 class="text-white">
                                        {{ $doctorCount }}
                                    </h4>
                                    <h6 class="text-white m-b-0">Total Dokter</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <canvas id="update-chart-3" height="50"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card bg-c-lite-green update-card">
                        <div class="card-block">
                            <div class="row align-items-end">
                                <div class="col-8">

                                    <h4 class="text-white">
                                        {{ $susterCount }}
                                    </h4>
                                    <h6 class="text-white m-b-0">Total Perawat
                                    </h6>
                                </div>
                                <div class="col-4 text-right">
                                    <canvas id="update-chart-4" height="50"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card bg-c-yellow update-card">
                        <div class="card-block">
                            <div class="row align-items-end">
                                <div class="col-8">

                                    <h4 class="text-white">
                                        {{ $obatCount }}
                                    </h4>
                                    <h6 class="text-white m-b-0">Total Obat</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <canvas id="update-chart-1" height="50"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card row col-lg-12">
                    <div class="card-block">
                        <!-- Row start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="sub-title">
                                    <h2>Welcome, {{ Auth::user()->username }}!!</h2>
                                </div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs md-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home3" role="tab">Profile</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#profile3" role="tab">Appointment</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>
                                <!-- Tab panes -->

                                <div class="tab-content card-block">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <h3>Edit Profile</h3>
                                        <br>
                                        @php
                                            $id = Auth::user()->id;
                                        @endphp
                                        <form id="main" method="post" action="{{ route('admin.update', $id) }}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Username</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="username" name="username"
                                                        placeholder="Username" value="{{ Auth::user()->username }}"
                                                        required="true">
                                                    <span class="messages"></span>
                                                </div>

                                                <label class="col-sm-2 col-form-label">First Name</label>
                                                <div class="col-sm-4">
                                                    <input type="text" id="firstname" name="firstname" class="form-control"
                                                        placeholder="First Name" value="{{ Auth::user()->firstname }}"
                                                        required="true">
                                                    <span class="messages"></span>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Last Name</label>
                                                <div class="col-sm-4">
                                                    <input type="text" id="lastname" name="lastname" class="form-control"
                                                        placeholder="Last Name" value="{{ Auth::user()->lastname }}"
                                                        required="true">
                                                    <span class="messages"></span>
                                                </div>

                                                <label class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-4">
                                                    <input type="text" id="email" name="email" class="form-control"
                                                        placeholder="Email" value="{{ Auth::user()->email }}"
                                                        required="true">
                                                    <span class="messages"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">No. Telp</label>
                                                <div class="col-sm-4">
                                                    <input type="text" id="tlp" name="tlp" class="form-control"
                                                        placeholder="No. Telp." value="{{ Auth::user()->tlp }}"
                                                        required="true">
                                                    <span class="messages"></span>
                                                </div>

                                                <label class="col-sm-2 col-form-label">Address</label>
                                                <div class="col-sm-4">
                                                    <input type="text" id="address" name="address" class="form-control"
                                                        placeholder="Address" value="{{ Auth::user()->address }}"
                                                        required="true">
                                                    <span class="messages"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                                <div class="col-sm-4">
                                                    <select id="gender" name="jeniskelamin" class="form-control">
                                                        <option
                                                            {{ Auth::user()->jeniskelamin == 'Laki-laki' ? 'selected' : '' }}
                                                            value="Laki-laki">
                                                            Laki-laki</option>
                                                        <option
                                                            {{ Auth::user()->jeniskelamin == 'Perempuan' ? 'selected' : '' }}
                                                            value="Perempuan">
                                                            Perempuan</option>
                                                    </select>
                                                    <span class="messages"></span>
                                                </div>

                                                <label class="col-sm-2 col-form-label">Address</label>
                                                <div class="col-sm-4">
                                                    @if (Auth::user()->avatar != null)
                                                        <label class="bmd-label-floating">Profile Picture | Selected
                                                            ({{ Auth::user()->avatar }})</label>
                                                    @else
                                                        <label class="bmd-label-floating">Profile Picture | Not Yet
                                                            Selected</label>
                                                    @endif
                                                    <input type="file" id="avatar" name="avatar" class="form-control"
                                                        placeholder="" value="{{ Auth::user()->avatar }}"
                                                        required="true">
                                                    <span class="messages"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" name="btn_submit"
                                                        class="btn btn-primary m-b-0">Submit</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                    <div class="tab-pane" id="profile3" role="tabpanel">
                                        <p class="m-0">
                                            <b>Appointment</b>

                                        <h3>Appointment records not found.. </h3>

                                        <h3>Last Appointment taken on -
                                        </h3>

                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row end -->
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5>Appoinment</h5>

                        </div>
                        <div class="card-block">
                            <div class="ct-chart1 ct-perfect-fourth"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5>Patient</h5>
                        </div>
                        <div class="card-block">
                            <div class="ct-chart1-patient ct-perfect-fourth"></div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
@endsection
