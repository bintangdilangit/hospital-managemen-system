@extends('layouts.master')
@section('title')
    Pasien
@endsection
@section('content')
    <div class="page-wrapper full-calender">
        <div class="page-body">
            <div class="row">


                <div class="row col-lg-12">
                    <h3><b>Add Pasien</b></h3>
                </div>
                <div class="row col-lg-12">Welcome to Abuya Kangean Hospital<br><br></div>

                <div class="card row col-lg-12">
                    <div class="card-block">
                        <!-- Row start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tab-content card-block">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <form action="{{ route('patient.update', $patient->id) }}" method="POST"
                                            data-parsley-validate enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group"> <label for="exampleInputEmail1">Name</label> <input
                                                    type="text" class="form-control" id="name" name="name"
                                                    placeholder="Full Name" value="{{ $patient->name }}" required="true">
                                            </div>
                                            <div class="form-group"> <label for="exampleInputEmail1">Email</label> <input
                                                    type="email" class="form-control" id="email" name="email"
                                                    placeholder="Email" value="{{ $patient->email }}" required="true">
                                            </div>
                                            <div class="form-group"> <label for="exampleInputEmail1">Mobile Number</label>
                                                <input type="" class="form-control" id="mobilenum" name="tlp"
                                                    placeholder="Mobile Number" value="{{ $patient->tlp }}"
                                                    required="true">
                                            </div>
                                            <div class="radio">

                                                <p style="padding-top: 20px; font-size: 15px"> <strong>Gender:</strong>
                                                    <label>
                                                        <input type="radio" name="jeniskelamin" id="gender"
                                                            value="Perempuan" checked="true">
                                                        Perempuan
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="jeniskelamin" id="gender"
                                                            value="Laki-laki">
                                                        Laki-laki
                                                    </label>
                                                </p>
                                                @if ($patient->jeniskelamin != null)
                                                    <label class="bmd-label-floating">Jenis Kelamin | Choosed
                                                        ({{ $patient->jeniskelamin }})</label>
                                                @else
                                                    <label class="bmd-label-floating">Jenis Kelamin | Not Yet
                                                        Choosed</label>
                                                @endif
                                            </div>
                                            <div class="form-group"> <label for="exampleInputEmail1">Details</label>
                                                <textarea type="text" class="form-control" id="details" name="details"
                                                    placeholder="Details" required="true" rows="12"
                                                    cols="4">{{ $patient->detail }}</textarea>
                                            </div>
                                            <br>
                                            <button type="submit" name="submit" class="btn btn-default">Update</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
