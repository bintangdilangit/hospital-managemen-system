@extends('layouts.master')
@section('title')
    Pendaftaran
@endsection
@section('content')
    <div class="page-wrapper full-calender">
        <div class="page-body">
            <div class="row">


                <div class="row col-lg-12">
                    <h3><b>Edit Pendaftaran</b></h3>
                </div>
                <div class="row col-lg-12">Welcome to Abuya Kangean Hospital<br><br></div>

                <div class="card row col-lg-12">
                    <div class="card-block">
                        <!-- Row start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tab-content card-block">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <form action="{{ route('pendaftaran.update', $pendaftaran->id) }}" method="POST"
                                            data-parsley-validate class="appointment-form">
                                            @csrf
                                            @method('PUT')
                                            <div class="row">
                                                @if ($pendaftaran->patients->name != null)
                                                    <label class="bmd-label-floating">Nama Pasien | Selected
                                                        ({{ $pendaftaran->patients->name }})</label>
                                                @else
                                                    <label class="bmd-label-floating">Nama Pasien | Not Yet
                                                        Selected</label>
                                                @endif
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <select name="patient_id" id="patient_id" required="true"
                                                            class="form-control">
                                                            <option value="">Pilih Pasien</option>
                                                            @foreach ($patient as $p)
                                                                <option value="{{ $p->id }}">
                                                                    {{ $p->name }} - {{ $p->email }}
                                                            @endforeach
                                                            </option>

                                                        </select>
                                                    </div>
                                                </div>
                                                @if ($pendaftaran->susters->name != null)
                                                    <label class="bmd-label-floating">Nama Suster | Selected
                                                        ({{ $pendaftaran->susters->name }})</label>
                                                @else
                                                    <label class="bmd-label-floating">Nama Suster | Not Yet
                                                        Selected</label>
                                                @endif
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="select-wrap">
                                                            <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                                            <select name="suster_id" id="suster_id" required="true"
                                                                class="form-control">
                                                                <option value="">Pilih Suster</option>
                                                                @foreach ($suster as $s)
                                                                    <option value="{{ $s->id }}">
                                                                        {{ $s->name }}
                                                                @endforeach
                                                                </option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($pendaftaran->doctors->name != null)
                                                    <label class="bmd-label-floating">Nama Dokter | Selected
                                                        ({{ $pendaftaran->doctors->name }})</label>
                                                @else
                                                    <label class="bmd-label-floating">Nama Dokter | Not Yet
                                                        Selected</label>
                                                @endif
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="select-wrap">
                                                            <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                                            <select name="doctor_id" id="doctor_id" required="true"
                                                                class="form-control">
                                                                <option value="">Pilih Doktor</option>
                                                                @foreach ($doctor as $d)
                                                                    <option value="{{ $d->id }}">
                                                                        {{ $d->name }}
                                                                @endforeach
                                                                </option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" name="submit" value="Buat pendaftaran"
                                                    class="btn btn-primary">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
