<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <title>@yield('title')</title>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Freelancer Nikhil Bhalerao">
    <meta name="author" content="Freelancer Nikhil Bhalerao">
    <link rel="icon" href="uploadImage/Logo/" type="image/x-icon">
    <link href="{{ asset('files/assets/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">



    <link rel="stylesheet" type="text/css"
        href="{{ asset('files/bower_components/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('files/assets/css/default-css.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('files/assets/css/font-awesome.min.css') }}">




    <link rel="stylesheet" type="text/css" href="{{ asset('files/assets/icon/icofont/css/icofont.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('files/assets/icon/feather/css/feather.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('files/assets/css/style.css') }}">


    <link rel="stylesheet" type="text/css" href="{{ asset('files/assets/css/jquery.mCustomScrollbar.css') }}">

    <link rel="stylesheet" href="popup_style.css">
</head>
<style>
    .social-icon-bar {
        z-index: 1111111;
        position: fixed;
        top: 49%;
        right: 0px;
    }

    .social-icon-bar a {
        display: block;
        text-align: center;
        padding: 16px;
        transition: all 0.3s ease;
        color: white;
        font-size: 20px;
    }

    .facebook {
        background: #3B5998;
        color: white;
    }

    .twitter {
        background: #29b200;
        color: white;
    }

    .google {
        background: #dd4b39;
        color: white;
    }

    .linkedin {
        background: #007bb5;
        color: white;
    }

    .youtube {
        background: #bb0000;
        color: white;
    }

    .website {
        background: #9400D3;
        color: white;
    }

</style>
{{-- <div class="social-icon-bar">
    <a href="https://www.facebook.com/freelancer.from.india" class="facebook" target="_blank"><i
            class="fa fa-facebook"></i></a>
    <a href="https://wa.me/919423979339" class="twitter" target="_blank"><i class="fa fa-whatsapp"></i></a>
    <a href="https://www.youtube.com/channel/UCnTEh3OFRS1wP0-Wqm2D-rA" class="youtube" target="_blank"><i
            class="fa fa-youtube"></i></a>
    <a href="https://www.nikhilbhalerao.com" class="website" target="_blank"><i class="fa fa-external-link"></i></a>

</div> --}}
{{-- Datatable --}}
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">

<body>
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Nav --}}
            @include('layouts.modules.navbar')
            {{-- End Nav --}}


            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">

                    {{-- Sidebar --}}
                    @include('layouts.modules.sidebar')
                    {{-- End Sidebar --}}



                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>




                {{-- Footer --}}
                @include('layouts.modules.footer')
                {{-- End Footer --}}
            </div>
            <!-- End Page wrapper  -->
        </div>
    </div>



    <script type="text/javascript" src="{{ asset('files/bower_components/jquery/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('files/bower_components/jquery-ui/js/jquery-ui.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('files/bower_components/popper.js/js/popper.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('files/bower_components/bootstrap/js/bootstrap.min.js') }}">
    </script>
    <script src="{{ asset('files/assets/js/pcoded.min.js') }}"></script>

    <script src="{{ asset('files/assets/js/vartical-layout.min.js') }}"></script>
    <script src="{{ asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('files/assets/js/jquery.mousewheel.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('files/assets/js/script.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('files/assets/js/script.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>


    <script>
        /*Threshold plugin for Chartist start*/
        var appointment = [];

        appointment.push();
        new Chartist.Line('.ct-chart1', {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Oct', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                appointment
            ]
        }, {
            showArea: false,

            axisY: {
                onlyInteger: true
            },
            plugins: [
                Chartist.plugins.ctThreshold({
                    threshold: 4
                })
            ]
        });

        var defaultOptions = {
            threshold: 0,
            classNames: {
                aboveThreshold: 'ct-threshold-above',
                belowThreshold: 'ct-threshold-below'
            },
            maskNames: {
                aboveThreshold: 'ct-threshold-mask-above',
                belowThreshold: 'ct-threshold-mask-below'
            }
        };

        //Second Chat
        var patient = [];

        new Chartist.Line('.ct-chart1-patient', {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Oct', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [patient]
        }, {
            showArea: false,

            axisY: {
                onlyInteger: true
            },
            plugins: [
                Chartist.plugins.ctThreshold({
                    threshold: 4
                })
            ]
        });

        var defaultOptions = {
            threshold: 0,
            classNames: {
                aboveThreshold: 'ct-threshold-above',
                belowThreshold: 'ct-threshold-below'
            },
            maskNames: {
                aboveThreshold: 'ct-threshold-mask-above',
                belowThreshold: 'ct-threshold-mask-below'
            }
        };

    </script>

    {{-- Datatable --}}
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    @stack('js')
</body>


</html>


<link rel="stylesheet" href="{{ asset('files/bower_components/chartist/css/chartist.css') }}" type="text/css"
    media="all">
<!-- Chartlist charts -->
<script src="{{ asset('files/bower_components/chartist/js/chartist.js') }}"></script>
<script src="{{ asset('files/assets/pages/chart/chartlist/js/chartist-plugin-threshold.js') }}"></script>
