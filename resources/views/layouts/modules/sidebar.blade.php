<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigatio-lavel">{{ Auth::user()->username }}</div>
        <ul class="pcoded-item pcoded-left-item">
            @if (Auth::user()->is_admin == 1)
                <li class="">
                    <a href="{{ route('admin.home') }}">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">Dashboard</span>
                    </a>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                        <span class="pcoded-mtext">Doctor</span>
                    </a>
                    <ul class="pcoded-submenu">

                        <li class="">
                            <a href="{{ route('doctor.create') }}">
                                <span class="pcoded-mtext">New Doctor</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{ route('doctor.index') }}">
                                <span class="pcoded-mtext">View Doctor</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                        <span class="pcoded-mtext">Suster</span>
                    </a>
                    <ul class="pcoded-submenu">

                        <li class="">
                            <a href="{{ route('suster.create') }}">
                                <span class="pcoded-mtext">New Suster</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{ route('suster.index') }}">
                                <span class="pcoded-mtext">View Suster</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                        <span class="pcoded-mtext">Room</span>
                    </a>
                    <ul class="pcoded-submenu">

                        <li class="">
                            <a href="{{ route('room.create') }}">
                                <span class="pcoded-mtext">New Room</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{ route('room.index') }}">
                                <span class="pcoded-mtext">View Room</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                        <span class="pcoded-mtext">Obat</span>
                    </a>
                    <ul class="pcoded-submenu">

                        <li class="">
                            <a href="{{ route('obat.create') }}">
                                <span class="pcoded-mtext">Add Obat</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{ route('obat.index') }}">
                                <span class="pcoded-mtext">List Obat</span>
                            </a>
                        </li>
                    </ul>
                </li>

            @else
                <li class="">
                    <a href="{{ route('home') }}">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">Dashboard</span>
                    </a>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather icon-edit"></i></span>
                        <span class="pcoded-mtext">Pendaftaran</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{ route('pendaftaran.create') }}">
                                <span class="pcoded-mtext">Pendaftaran Baru</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('pendaftaran.index') }}">
                                <span class="pcoded-mtext">List Pendaftaran</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                        <span class="pcoded-mtext">Patient</span>
                    </a>
                    <ul class="pcoded-submenu">

                        <li class="">
                            <a href="{{ route('patient.create') }}">
                                <span class="pcoded-mtext">Tambah Data Pasien</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{ route('patient.index') }}">
                                <span class="pcoded-mtext">List Pasien</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-submenu">
                    <a href="{{ route('bill.index') }}">
                        <span class="pcoded-micon"><i class="feather icon-file"></i></span>
                        <span class="pcoded-mtext">Pembayaran</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>
