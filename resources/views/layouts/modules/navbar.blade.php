<nav class="navbar header-navbar pcoded-header">
    <div class="navbar-wrapper">
        <div class="navbar-logo">

            <a href="dashboard.php">

                <div class="text-center">
                    <img class="profile-img" src="{{ asset('uploadImage/Logo/logo-for-hospital-system.jpg') }}"
                        style="width: 50%">
                </div>
            </a>
            <a class="mobile-options">
                <i class="feather icon-more-horizontal"></i>
            </a>
        </div>
        <div class="navbar-container container-fluid">
            <ul class="nav-left">
                <li>
                    <a href="#!" onclick="javascript:toggleFullScreen()">
                        <i class="feather icon-maximize full-screen"></i>
                    </a>
                </li>
            </ul>
            <ul class="nav-right">
                <li class="user-profile header-notification">
                    <div class="dropdown-primary dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown">
                            @if (Auth::user()->avatar != null)
                                <img class="profile-img" src="{{ asset('profile/' . Auth::user()->avatar) }}"
                                    style="width: 40px; border-radius:50%;">
                            @else
                                <img class="profile-img" src="{{ asset('uploadImage/Profile/profile.jpg') }}"
                                    style="width: 40px; border-radius:50%;">
                            @endif
                            <span></span>
                            <i class="feather icon-chevron-down"></i>
                        </div>
                        <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn"
                            data-dropdown-out="fadeOut">

                            <li>
                                @if (Auth::user()->is_admin == 1)
                                    <a href="{{ route('admin.home') }}">
                                        <i class="feather icon-user"></i> Profile
                                    </a>
                                @else
                                    <a href="{{ route('home') }}">
                                        <i class="feather icon-user"></i> Profile
                                    </a>
                                @endif

                            </li>

                            <li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                    <i class="feather icon-log-out"></i> Logout
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
