<?php

namespace Database\Factories;

use App\Models\Model;
use App\Models\Patient;
use App\Models\Room;
use App\Models\Suster;
use Illuminate\Database\Eloquent\Factories\Factory;

class SusterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Suster::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $rm = Room::pluck('id')->toArray();
        $jk = array('Laki-laki','Perempuan');
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'tlp' => $this->faker->phoneNumber,
            'jeniskelamin' => $this->faker->randomElement($jk),
            'room_id' => $this->faker->randomElement($rm),
        ];
    }
}