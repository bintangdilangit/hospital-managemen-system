<?php

namespace Database\Factories;

use App\Models\Level;
use App\Models\Model;
use App\Models\Room;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoomFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Room::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $lv = Level::pluck('id')->toArray();
        return [
            'nameRoom' => $this->faker->unique()->colorName,
            'priceRoom' => $this->faker->randomDigit,
            'level_id' => $this->faker->randomElement($lv),
        ];
    }
}