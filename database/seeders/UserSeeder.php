<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = [

            [

                'username'=> 'Halimatus Sakdiyah',
                'firstname'=>'Halimatus',
                'lastname'=>'Sakdiyah',
                'email'=>'halim@h.com',
                'jeniskelamin' => 'Perempuan',
                'is_admin'=>'1',
                'address' => 'Jl. Kangean',
                'tlp' => '08987233232',
                'password'=> bcrypt('111'),

            ],

            [
                'username'=> 'Santoso Jombang',
                'firstname'=>'Santoso',
                'lastname'=>'Jombang',
                'email'=>'santoso@s.com',
                'jeniskelamin' => 'Laki-laki',
                'is_admin'=>'0',
                'address' => 'Jl. Kangean',
                'tlp' => '0324823274',
                'password'=> bcrypt('111'),

            ],
            [
                'username'=> 'Mukidi Prob',
                'firstname'=>'Mukidi',
                'lastname'=>'Prob',
                'email'=>'mukidi@m.com',
                'jeniskelamin' => 'Laki-laki',
                'is_admin'=>'0',
                'address' => 'Jl. Kangean',
                'tlp' => '032443274',
                'password'=> bcrypt('111'),

            ],

        ];



        foreach ($user as $key => $value) {

            User::create($value);

        }

    }
}