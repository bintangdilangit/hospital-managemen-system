<?php

namespace Database\Seeders;

use App\Models\Suster;
use Illuminate\Database\Seeder;

class SusterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Suster::factory()->times(20)->create();
    }
}