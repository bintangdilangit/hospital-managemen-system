<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ObatSeeder::class,
            DoctorSeeder::class,
            LevelSeeder::class,
            RoomSeeder::class,
            SusterSeeder::class,
            PatientSeeder::class,
        ]);
    }
}