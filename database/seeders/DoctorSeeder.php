<?php

namespace Database\Seeders;
use App\Model;
use App\Models\Doctor;
use Illuminate\Database\Seeder;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Doctor::factory()->times(15)->create();
    }
    // {

    //     $doctor = [

    //         [

    //             'name' => 'doctor1',
    //             'email' => 'doc1@d.com',
    //             'tlp' => '048953445',
    //             'jeniskelamin' => 'Laki-laki',

    //         ],

    //         [
    //             'name' => 'doctor2',
    //             'email' => 'doc2@d.com',
    //             'tlp' => '048953445',
    //             'jeniskelamin' => 'Perempuan',
    //         ],
    //         [
    //             'name' => 'doctor3',
    //             'email' => 'doc3@d.com',
    //             'tlp' => '048953445',
    //             'jeniskelamin' => 'Laki-laki',
    //         ],
    //         [
    //             'name' => 'doctor4',
    //             'email' => 'doc4@d.com',
    //             'tlp' => '048953445',
    //             'jeniskelamin' => 'Perempuan',
    //         ],

    //     ];



    //     foreach ($doctor as $key => $value) {

    //         Doctor::create($value);

    //     }

    // }
}