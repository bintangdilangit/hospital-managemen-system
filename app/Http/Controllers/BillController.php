<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Booking;
use App\Models\Obat;
use Illuminate\Http\Request;

class BillController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $bill = Booking::orderBy('created_at', 'desc')->get();
        $obat = Obat::orderBy('created_at', 'desc')->get();
        return view('receptionist.bill.index', compact('bill'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pendaftar = Booking::orderBy('created_at', 'desc')->get();
        $obat = Obat::orderBy('created_at', 'desc')->get();
        return view('receptionist.bill.create',compact('pendaftar','obat'));
    }
    public function store(Request $request)
    {

        $bill = new Bill();
        $bk = Booking::orderBy('created_at', 'desc')->get();
        $booking = Booking::where('id',$request->booking_id);

        foreach ($bk as $b ){
            dd($bk->susters[$b]);

        }
        if ($request->booking_id == true) {

        }
        dd($request->booking_id);
        // dd($bill->totalBiaya = $request->bookings->susters->rooms->priceRoom + $request->obats->obatPrice);
        $bill->save();
        return redirect(route('bill.index'));
    }

}