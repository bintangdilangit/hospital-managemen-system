<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Doctor;
use App\Models\Obat;
use App\Models\Patient;
use App\Models\Suster;
use Illuminate\Http\Request;

class PendaftaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pendaftaran = Booking::orderBy('created_at', 'desc')->get();
        return view('receptionist.pendaftaran.index', compact('pendaftaran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $patient = Patient::orderBy('created_at', 'desc')->get();
        $suster = Suster::orderBy('created_at', 'desc')->get();
        $doctor = Doctor::orderBy('created_at', 'desc')->get();
        $obat = Obat::orderBy('created_at', 'desc')->get();
        return view('receptionist.pendaftaran.create',compact('patient','suster','doctor','obat'));
    }

    public function store(Request $request)
    {
        $pendaftaran = new Booking;
        $pendaftaran->patient_id = $request->patient_id;
        $pendaftaran->suster_id = $request->suster_id;
        $pendaftaran->doctor_id = $request->doctor_id;
        $pendaftaran->obat_id = $request->doctor_id;
        $pendaftaran->save();
        return redirect(route('pendaftaran.index'));
    }

    public function edit($id)
    {
        $patient = Patient::orderBy('created_at', 'desc')->get();
        $suster = Suster::orderBy('created_at', 'desc')->get();
        $doctor = Doctor::orderBy('created_at', 'desc')->get();
        $pendaftaran = Booking::where('id', $id)->first();
        $obat = Obat::orderBy('created_at', 'desc')->get();
        return view('receptionist.pendaftaran.edit', compact('pendaftaran','patient','suster','doctor','obat'));
    }
    public function update(Request $request, $id)
    {
        $pendaftaran = Booking::find($id);
        $pendaftaran->patient_id = $request->patient_id;
        $pendaftaran->suster_id = $request->suster_id;
        $pendaftaran->doctor_id = $request->doctor_id;
        $pendaftaran->obat_id = $request->doctor_id;
        $pendaftaran->update();
        return redirect(route('pendaftaran.index'));
    }
    public function destroy($id)
    {
        $pendaftaran = Booking::find($id);
        $pendaftaran->delete();
        return redirect(route('pendaftaran.index'));
    }
}