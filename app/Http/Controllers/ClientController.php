<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function updateRecep(Request $request, $id){
        $users = User::find($id);
        $users->update($request->all());
        if ($request->hasFile('avatar')){
            $request->file('avatar')->move('profile/',$request->file('avatar')->getClientOriginalName());
            $users->avatar = $request->file('avatar')->getClientOriginalName();
            $users->save();
        }
        return redirect(route('home'));
    }
}