<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Room;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $doctor = Doctor::orderBy('created_at', 'desc')->get();
        return view('admin.doctor.index', compact('doctor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.doctor.create');
    }
    public function store(Request $request)
    {
        if ($request->hasFile('ktp')){
            if ($request->hasFile('ijazah')) {
                $doctor = new Doctor();
                $doctor->name = $request->name;
                $doctor->email = $request->email;
                $doctor->tlp = $request->tlp;
                $doctor->jeniskelamin = $request->jeniskelamin;
                $request->file('ktp')->move('ktp/', $request->file('ktp')->getClientOriginalName());
                $doctor->ktp = $request->file('ktp')->getClientOriginalName();
                $request->file('ijazah')->move('ijazah/', $request->file('ijazah')->getClientOriginalName());
                $doctor->ijazah = $request->file('ijazah')->getClientOriginalName();
                $doctor->save();
                return redirect(route('doctor.index'));
            }
        }
    }

    public function edit($id)
    {
        $doctor = Doctor::where('id', $id)->first();
        return view('admin.doctor.edit', compact('doctor'));
    }


    public function update(Request $request, $id)
    {
        $doctor = Doctor::find($id);

        $doctor->name = $request['name'];
        $doctor->email = $request['email'];
        $doctor->tlp = $request['tlp'];
        $doctor->jeniskelamin = $request['jeniskelamin'];

        $doctor->update();
        return redirect(route('doctor.index'));
    }
    public function destroy($id)
    {
        $doctor = Doctor::find($id);
        $doctor->delete();
        return redirect(route('doctor.index'));
    }
}