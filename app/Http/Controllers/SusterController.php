<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\Suster;
use Illuminate\Http\Request;

class SusterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $suster = Suster::orderBy('created_at', 'desc')->get();
        return view('admin.suster.index', compact('suster'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = Room::orderBy('created_at', 'desc')->get();
        return view('admin.suster.create',compact('rooms'));
    }
    public function store(Request $request)
    {
        if ($request->hasFile('ktp')){
            if ($request->hasFile('ijazah')) {
                $suster = new Suster();
                $suster->name = $request->name;
                $suster->email = $request->email;
                $suster->tlp = $request->tlp;
                $suster->room_id = $request->room_id;
                $suster->jeniskelamin = $request->jeniskelamin;
                $request->file('ktp')->move('ktp/', $request->file('ktp')->getClientOriginalName());
                $suster->ktp = $request->file('ktp')->getClientOriginalName();
                $request->file('ijazah')->move('ijazah/', $request->file('ijazah')->getClientOriginalName());
                $suster->ijazah = $request->file('ijazah')->getClientOriginalName();
                $suster->save();
                return redirect(route('suster.index'));
            }
        }
    }

    public function edit($id)
    {
        $suster = Suster::where('id', $id)->first();
        return view('admin.suster.edit', compact('suster'));
    }


    public function update(Request $request, $id)
    {
        $suster = Suster::find($id);

        $suster->name = $request['name'];
        $suster->email = $request['email'];
        $suster->tlp = $request['tlp'];
        $suster->jeniskelamin = $request['jeniskelamin'];

        $suster->update();
        return redirect(route('suster.index'));
    }
    public function destroy($id)
    {
        $suster = Suster::find($id);
        $suster->delete();
        return redirect(route('suster.index'));
    }
}