<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    public function patients(){
        return $this->belongsTo(Patient::class, 'patient_id');
    }
    public function susters(){
        return $this->belongsTo(Suster::class, 'suster_id');
    }
    public function doctors(){
        return $this->belongsTo(Doctor::class, 'doctor_id');
    }
    public function obats(){
        return $this->belongsTo(Obat::class, 'obat_id');
    }
}